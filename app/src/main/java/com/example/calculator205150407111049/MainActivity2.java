package com.example.calculator205150407111049;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class MainActivity2 extends AppCompatActivity {

    TextView hasil, operasi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        String result = getIntent().getStringExtra("keyHasil");
        String operation = getIntent().getStringExtra("keyOperasi");

        hasil = findViewById(R.id.hasil);
        operasi = findViewById(R.id.operasi);

        hasil.setText("= " + result);
        operasi.setText(operation);
    }
}